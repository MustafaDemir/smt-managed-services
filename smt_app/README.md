Waar is deze repository voor bedoeld?

In deze app zitten er dashboards/reports m.b.t. rapportage doeleinden voor MS-klanten. 
Deze app moet worden geinstalleerd op de MC.

Om gebruik te maken van het app moeten er een aantal dingen worden gedaan.

1. Er moet eerst een index worden aangemaakt, namelijk "summary_smt".

2. De permissies van de app "splunk_monitoring_console" wijzigen in "Global". Dit omdat er searches  
   zijn die gebruik maken van de  KO's van de app.

3. LET OP!! 
   Voor standalone omgevingen dient er na installatie op de panel "Splunk User Overzicht" van  
   dashboards KPI en Rapportage de volgende search worden aangepast.
   Op regel 8 moet het volgende verwijderd worden:
   splunk_server_group=dmc_group_search_head | dedup title

